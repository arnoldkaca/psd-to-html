$(function () {
    $(document).scroll(function () {
        var $nav = $(".navbar-fixed-top");
        $nav.toggleClass('navbar-color-on-scroll', $(this).scrollTop() > $nav.height());
    });


    $("#wrapperButton").on("click", function(evt){
        evt.preventDefault();
        evt.stopPropagation();

        console.log($('#sidebar-wrapper').width());

        if($('#sidebar-wrapper').width() == 0){
            // $('#sidebar-wrapper').css("width", "50%");
            $('#sidebar-wrapper').animate({ 'width': '50%'}, 500);
            $('#navbar').animate({ 'marginLeft': '50%'}, 500);
        }
        if($('#sidebar-wrapper').width() > 0){
            // $('#sidebar-wrapper').css("width", "0");
            $('#sidebar-wrapper').animate({ 'width': '0'}, 500);
            $('#navbar').animate({ 'marginLeft': '0'}, 500);
        }

    });
});